import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import { Provider } from "react-redux";
import "./styles/style.scss";
import { store } from "./redux/store";
import { BrowserRouter } from "react-router-dom";
import { Loader } from "./components/index.tsx";

const Providers = () => {
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  return (
    <>
      <Provider store={store}>
        <BrowserRouter>{loading ? <Loader /> : <App/>}</BrowserRouter>
      </Provider>
    </>
  );
};

ReactDOM.createRoot(document.getElementById("root")!).render(
  <Providers />
);
