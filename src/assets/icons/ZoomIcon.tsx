
const ZoomIcon = () => {
  return (
<svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36" fill="none">
<circle cx="18" cy="18" r="13.5056" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<path d="M18.7503 12.7479H23.2522V17.2498" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<path d="M17.2497 23.2524H12.7478V18.7505" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
</svg>  )
}

export default ZoomIcon