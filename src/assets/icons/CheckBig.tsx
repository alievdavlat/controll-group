const CheckBig = (props:any) => {
  return (
    <svg
    xmlns="http://www.w3.org/2000/svg"
    width={89}
    height={66}
    fill="none"
    {...props}
  >
    <path
      stroke="#162850"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={15}
      d="M8 35.901 30.464 58l-.145-.143L81 8"
    />
  </svg>
  );
};

export default CheckBig;
