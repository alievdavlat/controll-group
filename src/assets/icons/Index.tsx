import ArrowDown from "./ArrowDown";
import BoxIcon from "./Box";
import Boyidgan from "./Boyidgan";
import CheckBig from "./CheckBig";
import CheckSmall from "./CheckSmall";
import Facebook from "./Facebook";
import FlagRu from "./FlagRu";
import FlagUz from "./FlagUz";
import Instagram from "./Instagram";
import Leneyka from "./Leneyka";
import List from "./List";
import MobileMenu from "./MobileMenu";
import Telegram from "./Telegram";
import ZoomIcon from "./ZoomIcon";

export {
  ArrowDown,
  Boyidgan,
  FlagRu,
  FlagUz,
  CheckBig,
  CheckSmall,
  Leneyka,
  MobileMenu,
  Instagram,
  Telegram,
  Facebook,
  ZoomIcon,
  BoxIcon,
  List,

}

