const FlagRu = () => {
  return (
    <div>
      <svg
        className="flag"
        viewBox="0 0 32 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0_97_1205)">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M0 0H32V32H0V0Z"
            fill="white"
          />
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M0 10.6665H32V31.9998H0V10.6665Z"
            fill="#0039A6"
          />
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M0 21.3335H32V32.0002H0V21.3335Z"
            fill="#D52B1E"
          />
        </g>
        <rect
          x="0.5"
          y="0.5"
          width="31"
          height="31"
          rx="15.5"
          stroke="#DCDCDC"
        />
        <defs>
          <clipPath id="clip0_97_1205">
            <rect width="32" height="32" rx="16" fill="white" />
          </clipPath>
        </defs>
      </svg>
    </div>
  );
};

export default FlagRu;
