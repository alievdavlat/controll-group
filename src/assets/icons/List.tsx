
const List = () => {
  return (
  <svg  viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
<g id="Group">
<path id="Path" d="M14.5836 15.1671H18.6686" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<path id="Path_2" d="M14.5836 15.1671H19.8358" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<rect id="Rectangle" x="3.49562" y="3.49561" width="21.0087" height="21.0087" rx="3" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<path id="Path_3" d="M24.5044 9.33138H3.49562" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<path id="Path_4" d="M8.16424 2.32837V4.66267" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<path id="Path_5" d="M19.8358 2.32837V4.66267" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<path id="Path_6" d="M8.16424 14.8472L9.23685 15.8136L11.3961 13.8691" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<path id="Path_7" d="M14.5836 19.8358H19.8358" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
<path id="Path_8" d="M8.16424 19.5159L9.23685 20.4823L11.3973 18.5378" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
</g>
</svg>

  )
}

export default List