import { SVGProps } from "react";

const Boyidgan = (props: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={46}
      height={46}
      fill="none"
      {...props}>
      <path
        stroke="#162850"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={1.5}
        d="M32.152 17.25H8.098a2.347 2.347 0 0 1-2.348-2.348V8.098A2.347 2.347 0 0 1 8.098 5.75h24.056A2.346 2.346 0 0 1 34.5 8.098v6.806a2.346 2.346 0 0 1-2.348 2.346v0ZM23 42.167h-3.833a1.917 1.917 0 0 1-1.917-1.917v-9.583c0-1.058.859-1.917 1.917-1.917H23c1.058 0 1.917.859 1.917 1.917v9.583A1.917 1.917 0 0 1 23 42.167Z"
        clipRule="evenodd"
      />
      <path
        stroke="#162850"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={1.5}
        d="M21.083 28.75v-3.833c0-1.058.859-1.917 1.917-1.917h15.333a1.917 1.917 0 0 0 1.917-1.917v-7.666a1.917 1.917 0 0 0-1.917-1.917H34.5"
      />
    </svg>
  );
};

export default Boyidgan;
