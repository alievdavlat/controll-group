const Leneyka = () => {
  return (
    <div>
      <svg
        width="46"
        height="46"
        viewBox="0 0 46 46"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M15.0494 23.2827L22.7173 30.9507C22.9915 31.2249 23.0736 31.6373 22.9252 31.9955C22.7767 32.3538 22.4271 32.5874 22.0393 32.5874H14.3714C13.8419 32.5874 13.4127 32.1581 13.4127 31.6286V23.9607C13.4127 23.5729 13.6462 23.2232 14.0045 23.0748C14.3628 22.9264 14.7752 23.0085 15.0494 23.2827Z"
          stroke="#162850"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M12.2895 6.87369L39.1263 33.7105C40.223 34.8073 40.5511 36.4568 39.9576 37.8898C39.364 39.3228 37.9656 40.2572 36.4146 40.2572H9.57773C7.45975 40.2572 5.7428 38.5402 5.7428 36.4222V9.58539C5.7428 8.03426 6.67713 6.63585 8.11019 6.04226C9.54325 5.44868 11.1928 5.77683 12.2895 6.87369Z"
          stroke="#162850"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M37.2126 31.7969L34.5048 34.5048"
          stroke="#162850"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M28.7524 28.7524L31.4603 26.0445"
          stroke="#162850"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M17.2476 17.2476L19.9555 14.5397"
          stroke="#162850"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M23 23L25.7079 20.2921"
          stroke="#162850"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M11.4952 11.5458L14.2285 8.81262"
          stroke="#162850"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </div>
  );
};

export default Leneyka;
