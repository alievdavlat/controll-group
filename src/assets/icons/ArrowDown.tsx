
const ArrowDown = () => {
  return (
    <div>
      <svg
        width="24"
        height="36"
        viewBox="0 0 24 36"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          d="M24 21.1203C17.3726 21.1203 12 27.7822 12 36C12 27.7822 6.62742 21.1203 -9.23198e-07 21.1203M12 34.9372L12 5.24532e-07"
          stroke="black"
          stroke-width="1.5"
        />
      </svg>
    </div>
  );
};

export default ArrowDown;
