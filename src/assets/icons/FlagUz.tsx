const FlagUz = () => {
  return (
    <div>
      <svg
        className="flag"
        viewBox="0 0 26 26"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0_40_356)">
          <path d="M0 17.3335H26V26.0002H0V17.3335Z" fill="#1EB53A" />
          <path d="M0 0H26V8.66667H0V0Z" fill="#0099B5" />
          <path d="M0 8.31982H26V17.6798H0V8.31982Z" fill="#CE1126" />
          <path d="M0 8.83984H26V17.1598H0V8.83984Z" fill="white" />
        </g>
        <rect
          x="0.5"
          y="0.5"
          rx="12.5"
          stroke="#DCDCDC"
        />
        <defs>
          <clipPath id="clip0_40_356">
            <rect width="26" height="26" rx="13" fill="white" />
          </clipPath>
        </defs>
      </svg>
    </div>
  );
};

export default FlagUz;
