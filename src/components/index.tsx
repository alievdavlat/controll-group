import AboutUs from "./sections/AboutUs";
import Advantages from "./sections/Advantages";
import Contact from "./sections/Contact";
import Footer from "./sections/Footer";
import Header from "./sections/Header";
import Hero from "./sections/Hero";
import Services from "./sections/Services";
import Sidebar from "./sections/Sidebar";
import Works from "./sections/Works";
import CustomModal from "./ui/CustomModal";
import VideoSection from "./sections/VideoSection";

// ui 
import LanguageModal from "./ui/LanguageModal";
import Loader from "./ui/Loader";
import FeedbackModal from "./ui/FeedbackModal"
import SuccessLoader from "./ui/SuccessLoader";
import ZoomModal from "./ui/ZoomModal";


export {
  AboutUs, 
  Advantages,
  Contact,
  Footer,
  Header,
  Hero,
  Services,
  Works,
  Sidebar,
  CustomModal,
  LanguageModal,
  Loader,
  FeedbackModal,
  SuccessLoader,
  ZoomModal,
  VideoSection
}