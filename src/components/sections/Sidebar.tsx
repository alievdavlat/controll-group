import React from 'react'
import { navbaritems } from '../../constants';
import { BsTelephone } from 'react-icons/bs';
import { FaTelegramPlane } from 'react-icons/fa';
import { BiLogoInstagramAlt } from 'react-icons/bi';
import { FaSquareFacebook } from 'react-icons/fa6';
import FlagRu from '../../assets/icons/FlagRu';
import FlagUz from '../../assets/icons/FlagUz';
import storage from '../../utils/helpers/storage';
import { IoMdClose } from "react-icons/io";



type Props = {
  child:any;
  setChild:(child:any) => void;
  setOpen:(open:boolean) => void;
  setOpenSidebar:(openSidebar:boolean) => void;
  openSidebar:boolean;
}
interface INavbarItem {
  id:number;
  title_ru:string;
  title_uz:string;
  path:string;
}


const Sidebar:React.FC<Props> = ({setChild, setOpen, setOpenSidebar}) => {


  const language = storage.getItem('language')


  return (
    <div className='sidebar'>

      <span className='sidebar_closeicon' onClick={() => setOpenSidebar(false)}>
      <IoMdClose />
      </span>

        <ul className='sidebar_nav'>
        {
            navbaritems?.map((item:INavbarItem) => (
          <li key={item?.id}>
            <a href={`3${item?.path}`}>
                {language && language === 'uz' ? item?.title_uz : item?.title_ru}
            </a>
          </li>
            ))
          }
        </ul>

        <div className='sidebar_number'>
        <a href="tel:+998977777727">
        <BsTelephone className="sidebar_number_icon" />
        </a>
        <a href="tel:+998977777727">
        +998 (97) 777 77 27
        </a>
        </div>

        
        <div className='sidebar_icons'>
        <a href='#' target="_blank" className='telegramIcon'>
        <FaTelegramPlane size={25}   className='telegramIcon_icon'/>
        </a>

        <a  href='#' target="_blank" className='instagramIcon'>
        <BiLogoInstagramAlt size={25}   className='instagramIcon_icon'/>
        </a>

        <a  href='#' target="_blank" className='facebookicon'>
        <FaSquareFacebook  size={25}   className='facebookicon_icon' />
        </a>

        <div className='sidebar_icons_language'>
          {
            language === 'uz'
            ? <span onClick={() => {
              setOpen(true)
              setChild('language')
            }}><FlagUz/></span>
            : <span onClick={() => {
              setOpen(true)
              setChild('language')
            }}><FlagRu/></span>
          }
        </div>

        </div>
    </div>
  )
}

export default Sidebar