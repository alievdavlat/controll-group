import React, { useEffect, useState } from 'react'
import storage from '../../utils/helpers/storage'
import { api } from '../../services'

type Props = {
    setOpen: (open: boolean) => void;
    setChild: (child: any) => void;

}

const Services: React.FC<Props> = ({setOpen, setChild}) => {
    const language = storage.getItem('language')
    const [servicesData, setServicesData] = useState<any>([]);
    const [servicesTitle, setServicesTitle] = useState<any>({})
  
    const getData = async () => {
      const result = await api.get(`services`);
      const servicesTitleResult = await api.get(`layout/titles`);
      
      setServicesTitle(servicesTitleResult?.data?.layout?.titles?.services)
      setServicesData(result?.data?.services);
    };
  

    
    
    
    useEffect(() => {
      getData();
    }, []);


    return (
        <section className='services' id='services'>
            <div className='services__title'>
                <h3>
                    {
                        language === 'uz'
                            ? servicesTitle?.title_uz
                            : servicesTitle?.title_ru
                    }
                </h3>
            </div>
            <div className='services__content'>
                <div 
                 className='services__content__wrapper'
                >

                {
                    servicesData.map((item:any) => (
                        <div
                        className='services__content-item' key={item?._id}
                        >
                                <div className="services__content-item--left">
                                    <div className='services__content-item--article'>
                                        {language === 'uz' ? item?.service_type_uz :  item?.service_type_ru}
                                    </div>
                                    <div className="services__content-item--left__header">
                                        <h3>
                                            {language === 'uz' ? item?.title_uz : item?.title_ru}
                                        </h3>

                                        <p>
                                            {language === 'uz' ? item?.description_uz : item?.description_ru}
                                        </p>
                                        {/*<div className='services__content-item--left__header_icons'>*/}
                                        {/*    <span>*/}
                                        {/*      <Leneyka/>  {language === 'uz' ? item?.subtitle_one_uz : item?.subtitle_one_ru}*/}
                                        {/*    </span>*/}

                                        {/*    <span>*/}
                                        {/*      <Boyidgan/> {language === 'uz' ? item?.subtitle_two_uz : item?.subtitle_two_ru}*/}
                                        {/*    </span>*/}
                                        {/*</div>*/}
                                    </div>

                                    <button onClick={() => {
                                        setOpen(true)
                                        setChild('feedback')
                                    }}>
                                        {
                                            language === 'uz' ? 'Ariza qoldirish': 'Оставить заявку'  
                                        }
                                    </button>
                                </div>
                                            
                                <img  className="services__content-item--right" src={ item?.img || ''} alt="1"/>
                            </div>
                        ))
                    }
                </div>
            </div>
        </section>
    )
}

export default Services