import React from 'react'
import footerLogo from '../../assets/logo.svg'
import {BsTelephone} from 'react-icons/bs'
import {CiLocationOn} from "react-icons/ci";
import {FaTelegramPlane} from 'react-icons/fa';
import {BiLogoInstagramAlt} from 'react-icons/bi';
import {FaSquareFacebook} from 'react-icons/fa6';
import storage from '../../utils/helpers/storage';
import { api } from '../../services';


type Props = {
    setOpen: (open: boolean) => void;
    setChild: (child: string) => void;
}

const Footer: React.FC<Props> = ({setChild, setOpen}) => {
    const language = storage.getItem('language')
    const [mainData, setMainData] = React.useState<any>({})

    const getData = async () => {
        const projectsTitleResult = await api.get(`layout/main`);
        setMainData(projectsTitleResult?.data?.layout?.main);
        
    }

    React.useEffect(() => {
        getData()
    }, [])

    

    return (
        <div className='footer'>
            <div className="footer__left">
                <div className='footer__left-logo'>
                    <img src={footerLogo} alt="logo"/>
                </div>

                <button onClick={() => {
                    setChild('feedback')
                    setOpen(true)
                }}>
                   {
                    language === 'uz' ? "Sorov Jonatish" : 'Отправить заявку'
                   }
                </button>
            </div>

            <div className="footer__right">
                <div className="footer__right-columns">
                    <ul>
                        <li>
                            <a href="#about">
                                {
                                    language == 'uz' ? 'Kompaniya haqida' :" О Компании"
                                }
                            </a>
                        </li>
                        <li>
                            <a href="#services">
                               
                                {
                                    language == 'uz' ? 'Ish turlari' :' Виды работ'
                                }
                            </a>
                        </li>
                        <li>
                            <a href="#projects">
                               {
                                language === 'uz' ? 'Tugallangan loyihalar' :" Реализованные проекты"
                               }
                            </a>
                        </li>
                    </ul>

                    <ul>
                        <li>
                            <a href="#advantages">
                                {
                                    language === 'uz' ? 'Noyob afzalliklar':"Уникальные преимущества"
                                }
                            </a>
                        </li>
                        <li>
                            <a href="#review">
                              {
                                language === 'uz' ? "Mijozlarning sharhlari" : 'Отзывы клиентов'
                              }
                            </a>
                        </li>
                    </ul>

                    <button onClick={() => {
                        setChild('feedback')
                        setOpen(true)
                    }}>
                       {
                    language === 'uz' ? "Ariza yuborish" : 'Отправить заявку'
                   }
                    </button>

                    <div className="footer__column">
                        <a className='footer__number' href={`tel:${mainData?.primary_phone}`}>
                            <BsTelephone className="footer__number-icon"/>
                            <span>{mainData?.primary_phone}</span>
                        </a>
                        <a className='footer__number' href={`tel:+${mainData?.secondary_phone}`}>
                            <BsTelephone className="footer__number-icon"/>
                            <span>{mainData?.secondary_phone}</span>
                        </a>

                        <div className='footer__right-address'>
                            <CiLocationOn size={22}/>
                            <span>
                       {
                        language === 'uz' ? mainData?.address_uz : mainData?.address_ru
                       }
                        </span>

                        </div>

                        <div className='footer__right-icons'>
                            <a href='#' target="_blank" className='telegramIcon'>
                                <FaTelegramPlane size={25} className='telegramIcon_icon'/>
                            </a>

                            <a href='#' target="_blank" className='instagramIcon'>
                                <BiLogoInstagramAlt size={25} className='instagramIcon_icon'/>
                            </a>

                            <a href='#' target="_blank" className='facebookicon'>
                                <FaSquareFacebook size={25} className='facebookicon_icon'/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer