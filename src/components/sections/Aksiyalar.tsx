import { useEffect, useState } from "react";
import { api } from "../../services";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper/modules";
import moment from 'moment'

import "swiper/css";
import 'swiper/css/free-mode';
import 'swiper/css/navigation';
import storage from "../../utils/helpers/storage";

interface IStockData {
  _id: string;
  description_uz: string;
  description_ru: string;
  image: string;
  createdAt: string;
  updatedAt: string;
}

const Aksiyalar = () => {
  const [Aksiyalar, setAksiyalar] = useState<IStockData[]>([
    {
      _id: "",
      description_uz: "",
      description_ru: "",
      image: "",
      createdAt: "",
      updatedAt: "",
    },
  ]);
  const [stocksTitle, setStocksTitle] = useState<any>({})
  const [swiperRef, setSwiperRef] = useState<any>(null);
  const language = storage.getItem("language");

  const prevHandler = () => {
    swiperRef.slidePrev();
  };

  const nextHandler = () => {
    swiperRef.slideNext();
  };

  const getData = async () => {
    const result = await api.get(`stocks`);
    const stocksTitleResult = await api.get(`layout/titles`);
      
    setStocksTitle(stocksTitleResult?.data?.layout?.titles?.stocks)  
    setAksiyalar(result?.data?.stocks);
  };

  useEffect(() => {
    getData();
  }, []);


  return (
    <div className="stocks" id="stocks">
      <div className="stocks_top container">
        <h1>
          {
              language === 'uz'
                  ? stocksTitle?.title_uz
                  : stocksTitle?.title_ru
          }
          </h1>
        <div className="stocks_top--arrows">
          <span onClick={prevHandler}>
            <IoIosArrowBack />
          </span>
          <span onClick={nextHandler}>
            <IoIosArrowForward />
          </span>
        </div>
      </div>

      <div className="stocks_bottom">
        <div className="container">
          <Swiper
            slidesPerView={3}
            className="works_swipper"
            loop={true}
            speed={2000}
            spaceBetween={0}
            onSwiper={(swiper) => setSwiperRef(swiper)}
            modules={[Autoplay]}
            autoplay={{
              delay: 2500,
              disableOnInteraction: false,
            }}
            breakpoints={{
              320: {
                slidesPerView: 1,
                spaceBetween: 20,
              },
              380: {
                slidesPerView: 1,
                spaceBetween: 20,
              },

              640: {
                slidesPerView: 2,
                spaceBetween: 20,
              },
              768: {
                slidesPerView: 2,
                spaceBetween: 0,
              },
              992: {
                slidesPerView: 3,
                spaceBetween: 20,
              },
              1024: {
                slidesPerView: 3,
                spaceBetween: 20,
              },
            }}>
            {Aksiyalar?.map((item: any) => (
              <SwiperSlide key={item?._id}>
                <div className="stocks_bottom--item" key={item?._id}>
                  <img src={item?.image ? item?.image : ""} alt="stocks" />
                  <div className="stocks_bottom--item__date">
                    <p>{moment(item?.createdAt).calendar()}</p>
                  </div>
                  <div className="stocks_bottom--item__text">
                    <p>
                      {language === "uz"
                        ? item?.description_uz
                        : item?.description_ru}
                    </p>
                  </div>
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </div>
  );
};

export default Aksiyalar;
