import { useEffect, useState } from "react";
import advantgesImg from "../../assets/images/advantages/Rectangle 3196.jpg";
import { api } from "../../services";
import storage from "../../utils/helpers/storage";

interface IAdvantagesData {
    _id: string,
    title_ru: string,
    title_uz: string,
    description_uz: string,
    description_ru: string,
    img: string,
    createdAt: string,
    updatedAt:string
}



const Advantages = () => {
    const language = storage.getItem("language");
    const [advanatgesData , setAdvantagesData] = useState<any>([])
    const [advantagesTitle, setAdvantagesTitle] = useState<any>({})

    const getData = async () => {
        const result = await api.get(`advantages`)
        const advantagesTitleResult = await api.get(`layout/titles`);
      
        setAdvantagesTitle(advantagesTitleResult?.data?.layout?.titles?.advantage)  
        setAdvantagesData(result?.data?.advanatges)
    }   

    useEffect(() => {
        getData()
    }, [])

    return (
        <div className="advantages" id="advantages">
            <div className="advantages__top">
                <h2>
                    {
                        language === 'uz'
                            ? advantagesTitle?.title_uz
                            : advantagesTitle?.title_ru
                    }
                </h2>
            </div>
            <div
                className="advantages__bottom"
                >
                {
                    advanatgesData.map((item:IAdvantagesData, index:number) => (
                        <div
                        className="advantages__bottom-item" key={item?._id}
                        >

                            <h4>0{index + 1}</h4>

                            <p>{language === 'uz' ? item?.title_uz :item?.title_ru}</p>

                            <img src={item?.img || advantgesImg} alt="advantages"/>
                            <div className="pocket">
                                <p>
                                    {language === 'uz' ? item?.description_uz : item?.description_ru}
                                </p>
                            </div>
                    </div>
                    ))
                }
            </div>

        </div>
    );
};

export default Advantages;
