import { useEffect, useRef, useState } from "react";
import { MdClose } from "react-icons/md";
import { IoVolumeHigh } from "react-icons/io5";
import { IoMdVolumeOff } from "react-icons/io";
import { api } from "../../services";

const VideoSection = () => {
  const videoRef = useRef<any>(null);
  const [collapse, setCollapse] = useState(false);
  const [addSound, setSouond] = useState(true);
  const [videoData, setVideoData] = useState('')
  const [isLoading, setIsloading] = useState(false)

  const handleVideoEnd = () => {
    videoRef.current.play();
  };

  

  const getData = async () => {
   try {
    setIsloading(true)
    const result = await api.get(`layout/video`);
    
    setVideoData(result?.data?.layout?.video);
    
    setTimeout(() => {
      setIsloading(false)
    }, 2000);

   } catch (err) {
    setIsloading(false)
   }
  };


  useEffect(() => {
    getData();
  }, []);


  return (
   <>
   {
    isLoading  ? '' 
    : <div className={`videoSection ${collapse && 'active_videoSection'}`}>

    {
     collapse &&  <div className="videoSection_controls">
     <span onClick={() => {
       setCollapse(false)
       setSouond(true)
     }}>
       <MdClose />
     </span>
     {!addSound ? (
       <span onClick={() => setSouond(true)}>
         <IoVolumeHigh />
       </span>
     ) : (
       <span onClick={() => setSouond(false)}>
         <IoMdVolumeOff />
       </span>
     )}
   </div>
    }

     <video
       onClick={() => {
         setCollapse(true)
         setSouond(false)
       }}
       ref={videoRef}
       autoPlay={true}
       muted={addSound}
       loop
       onEnded={handleVideoEnd}
       >
       <source
         src={videoData}
       />
     </video>
   </div>
   }
   </>
  );
};

export default VideoSection;
