import React from 'react'
import {BsTelephone} from "react-icons/bs";
import headerLogo from '../../assets/logo.svg'
import {navbaritems} from '../../constants';
import storage from '../../utils/helpers/storage';
import {FaTelegramPlane} from "react-icons/fa";
import {BiLogoInstagramAlt} from "react-icons/bi";
import {FaSquareFacebook} from "react-icons/fa6";
import {BiMenuAltLeft} from "react-icons/bi";
import FlagUz from '../../assets/icons/FlagUz';
import FlagRu from '../../assets/icons/FlagRu';
import {Link} from 'react-router-dom';
import {motion} from "framer-motion";
import {navVariants} from '../../utils/motion'
import { api } from '../../services';


type Props = {
    child: any;
    setChild: (child: any) => void;
    setOpen: (open: boolean) => void;
    setOpenSidebar: (openSidebar: boolean) => void;
    openSidebar: boolean;
}


interface INavbarItem {
    id: number;
    title_ru: string;
    title_uz: string;
    path: string;
}

const Header: React.FC<Props> = ({setChild, setOpen, openSidebar, setOpenSidebar}) => {

    const language = storage.getItem('language')
    const [mainData, setMainData] = React.useState<any>({})
    const [activeItem, setActiveItem] = React.useState<any>(null)

    const getData = async () => {
        const projectsTitleResult = await api.get(`layout/main`);
        setMainData(projectsTitleResult?.data?.layout?.main);
        
    }

    React.useEffect(() => {
        getData()
    }, [])

    return (
        <motion.div
            variants={navVariants}
            initial="hidden"
            whileInView="show"
            className={`header`}
        >
            <nav className='navbar'>
                <Link to={'/'} className='navbar__logo'>
                    <img src={headerLogo} alt="logo"/>
                </Link>
                <ul className='navbar__nav'>
                    {
                        navbaritems?.map((item: INavbarItem, index:number) => (
                            <li key={item.id} className={activeItem  === index ? 'navActive' :""} onClick={() => setActiveItem(index)}>
                                <a href={`#${item?.path}`}>
                                    {language && language === 'uz' ? item?.title_uz : item?.title_ru}
                                </a>
                            </li>
                        ))
                    }
                </ul>
                <div className="navbar__right">
                    <div className='navbar__icons'>
                        <a href='#' target="_blank" className='telegramIcon'>
                            <FaTelegramPlane size={25} className='telegramIcon__icon'/>
                        </a>

                        <a href='#' target="_blank" className='instagramIcon'>
                            <BiLogoInstagramAlt size={25} className='instagramIcon__icon'/>
                        </a>

                        <a href='#' target="_blank" className='facebookicon'>
                            <FaSquareFacebook size={25} className='facebookicon__icon'/>
                        </a>

                        <div className='navbar__icons-language'>
                            {
                                language === 'uz'
                                    ? <span onClick={() => {
                                        setOpen(true)
                                        setChild('language')
                                    }}><FlagUz/></span>
                                    : <span onClick={() => {
                                        setOpen(true)
                                        setChild('language')
                                    }}><FlagRu/></span>
                            }
                        </div>
                    </div>

                    <div className='mobile__menu' onClick={() => setOpenSidebar(!openSidebar)}>
                        <BiMenuAltLeft className='mobile__menu-icon'/>
                    </div>

                    <div className='navbar__number'>
                        <a href={`tel:+${mainData?.primary_phone}`}>
                            <BsTelephone className="navbar__number-icon"/>
                        </a>
                        <a href={`tel:+${mainData?.primary_phone}`} className="navbar__number-num">
                        {mainData?.primary_phone}
                        </a>
                    </div>
                </div>
            </nav>
        </motion.div>
    )
}

export default Header