//@ts-nocheck

import BoxIcon from "../../assets/icons/Box";
import ZoomIcon from "../../assets/icons/ZoomIcon";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper/modules";
import "swiper/css";
import { FC, useEffect, useState } from "react";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import List from "../../assets/icons/List";
import storage from "../../utils/helpers/storage";
import { api } from "../../services";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";
import { ZoomModal } from "..";

type Porps = {
  zoomImages:any;
  setZoomImages:(zoomImages:any) => void;
  setZoomModalOpen:(zommModalOpen:boolean) => void
}

const Works:FC<Porps> = ({setZoomImages, zoomImages, setZoomModalOpen}) => {
  const language = storage.getItem("language");
  const [worksData, setWorksData] = useState<any>();
  const [swiperRef, setSwiperRef] = useState<any>(null);
  const [projectsTitle, setProjectsTitle] = useState<any>({})

  const [opne ,setOpen] = useState(true)
  const prevHandler = () => {
    swiperRef.slidePrev();
  };

  const nextHandler = () => {
    swiperRef.slideNext();
  };

  const getData = async () => {
    const result = await api.get(`projects`);
    const projectsTitleResult = await api.get(`layout/titles`);
      
    setProjectsTitle(projectsTitleResult?.data?.layout?.titles?.projects)  
    setWorksData(result?.data?.projects);

  };


  useEffect(() => {
    getData();
  }, []);


  return (


    <div className="works" id="projects">
      <div className="works_top container">
        <h2>
          {
              language === 'uz'
                  ? projectsTitle?.title_uz
                  : projectsTitle?.title_ru
          }
        </h2>
        <div className="works_top--arrows">
          <span onClick={prevHandler}>
            <IoIosArrowBack />
          </span>
          <span onClick={nextHandler}>
            <IoIosArrowForward />
          </span>
        </div>
      </div>

      <div className="works__bottom">
        <div className="container">
          <Swiper
            slidesPerView={3}
            className="works_swipper"
            loop={false}
            speed={2000}
            spaceBetween={40}
            // centeredSlides={true}
            onSwiper={(swiper) => setSwiperRef(swiper)}
            modules={[Autoplay]}
            // autoplay={{
            //   delay: 2500,
            //   disableOnInteraction: false,
            // }}
            breakpoints={{
              320: {
                slidesPerView: 1,
                spaceBetween: 20,
              },
              380: {
                slidesPerView: 1,
                spaceBetween: 20,
              },

              640: {
                slidesPerView: 2,
                spaceBetween: 20,
              },
              768: {
                slidesPerView: 2,
                spaceBetween: 0,
              },
              992: {
                slidesPerView: 3,
                spaceBetween: 20,
              },
              1024: {
                slidesPerView: 2.8,
                spaceBetween: 20,
              },
            }}>
            {worksData && worksData?.map((item: any, index:number) => (
              <SwiperSlide key={item?._id}>
                <div className="works__bottom-item" key={item?._id}>
                  <div className="works__bottom-item__coursel ">
                    <div className="works_bottom__item__coursel--img">
                    <ImageGallery items={item?.images}  />
                      <span className="zoomIcon" onClick={() => {
                        setZoomImages(item?.images)
                          setZoomModalOpen(true)
                      }} >
                        <ZoomIcon />
                      </span>
                    </div>
                  </div>
                    <div className="works__bottom-pocket">
                        <div className="works__bottom-item__title">
                            <h4>{item?.address}</h4>
                            <div className="works__bottom-item__title-line"></div>
                        </div>

                        <div className="works__bottom-item-info">
                            <div className="works__bottom-item-info_item">
                      <span>
                        {language === "uz"
                            ? "Yakunlash muddati"
                            : "Срок выполнения"}
                      </span>
                                <div>
                                    <div>
                                        <List />
                                    </div>
                                    <span>
                          {language === "uz"
                              ? item?.deadline_uz
                              : item?.deadline_ru}
                        </span>
                                </div>
                            </div>

                            <div className="works__bottom-item-info_item">
                                <span>{language === "uz" ? "Maydoni" : "Площадь"}</span>
                                <div>
                                    <div>
                                        <BoxIcon />
                                    </div>
                                    <span>
                          {item?.measurment}
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>

      <div className="works__bottom--arrows">
          <span onClick={prevHandler}>
            <IoIosArrowBack />
          </span>
          <span onClick={nextHandler}>
            <IoIosArrowForward />
          </span>
        </div>
    </div>

  );
};

export default Works;
