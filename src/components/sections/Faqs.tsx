import { useEffect, useState } from "react";
import { api } from "../../services";
import storage from "../../utils/helpers/storage";
import { MdKeyboardArrowUp } from "react-icons/md";
import { MdKeyboardArrowDown } from "react-icons/md";

interface IFaqData {
  _id: string;
  answer_uz: string;
  answer_ru: string;
  question_uz: string;
  question_ru: string;
}

const Faqs = () => {
  const lang = storage.getItem("language");
  const [faqsTitle, setFaqsTitle] = useState<any>({})
  const [activeIndex, setActiveIndex] = useState(0); // Set the initial active index to 0
  const [faqData, setFaqData] = useState<IFaqData[]>([
    {
      _id: "",
      answer_uz: "",
      answer_ru: "",
      question_uz: "",
      question_ru: "",
    },
  ]);

  const getData = async () => {
    const result = await api.get(`faqs`);
    const faqsTitleResult = await api.get(`layout/titles`);
      
    setFaqsTitle(faqsTitleResult?.data?.layout?.titles?.reviws)      
    setFaqData(result?.data?.faqs);
  };

  useEffect(() => {
    getData();
  }, []);

  const handleItemClick = (index: number) => {
    // @ts-ignore
    setActiveIndex((prevIndex) => (prevIndex === index ? null : index));
  };

  return (
      <div className="faq" id="faq">
        <div className="faq_top container">
          <h3>
            {
                lang === 'uz'
                    ? faqsTitle?.title_uz
                    : faqsTitle?.title_ru
            }
          </h3>
        </div>

        <div className="faq_bottom container">
          <div className="faq_bottom-column">
            {faqData?.map((item: IFaqData, index: number) => (
                (index % 2 === 0) && (
                    <div
                        onClick={() => handleItemClick(index)}
                        className="faq_bottom--item"
                        key={item?._id}
                    >
                      <div className="faq_bottom--item_top">
                        <p>{lang === "uz" ? item?.answer_uz : item?.answer_ru}</p>{" "}
                        <span
                            style={{
                              background: `${index === activeIndex ? "#162850" : ""}`,
                            }}
                        >
              {index === activeIndex ? (
                  <MdKeyboardArrowUp size={22} fill={"white"} />
              ) : (
                  <MdKeyboardArrowDown size={22} fill={"black"} />
              )}
            </span>
                      </div>

                      <div
                          className={`faq_bottom--item_body ${
                              index === activeIndex && "body_active"
                          }`}
                      >
                        <div className="faq_bottom--item_pocket">
                          {lang === "uz" ? item?.question_uz : item?.question_ru}
                        </div>
                      </div>
                    </div>
                )
            ))}
          </div>
          <div className="faq_bottom-column">
            {faqData?.map((item: IFaqData, index: number) => (
                (index % 2 !== 0) && (
                    <div
                        onClick={() => handleItemClick(index)}
                        className="faq_bottom--item"
                        key={item?._id}
                    >
                      <div className="faq_bottom--item_top">
                        <p>{lang === "uz" ? item?.answer_uz : item?.answer_ru}</p>{" "}
                        <span
                            style={{
                              background: `${index === activeIndex ? "#162850" : ""}`,
                            }}
                        >
              {index === activeIndex ? (
                  <MdKeyboardArrowUp size={22} fill={"white"} />
              ) : (
                  <MdKeyboardArrowDown size={22} fill={"black"} />
              )}
            </span>
                      </div>

                      <div
                          className={`faq_bottom--item_body ${
                              index === activeIndex && "body_active"
                          }`}
                      >
                        <div className="faq_bottom--item_pocket">
                          {lang === "uz" ? item?.question_uz : item?.question_ru}
                        </div>
                      </div>
                    </div>
                )
            ))}
          </div>
        </div>

      </div>
  );
};

export default Faqs;
