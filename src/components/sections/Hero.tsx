import React, { useEffect, useState } from 'react'
import heroImg from '../../assets/images/hero/hero-img-1344.png'
import storage from '../../utils/helpers/storage'
import {FaCheck} from "react-icons/fa";
import {FaPhoneAlt} from "react-icons/fa";
import { api } from '../../services';
import arrow from '../../assets/images/arrow.png'
interface IHeroData {
    image: string,
    hero_services_uz: string[],
    title_ru: string,
    title_uz: string,
    subtitle_ru: string,
    subtitle_uz: string,
    hero_services_ru:string[]
}

type Props = {
    setOpen: (open: boolean) => void;
    setChild: (child: any) => void;

}
const Hero: React.FC<Props> = ({setChild, setOpen}) => {
    const [mainData, setMainData] = React.useState<any>({})
    const language = storage.getItem('language')
    const [herData , setHeroData] = useState<IHeroData>({
    image: '',
    hero_services_uz: [],
    title_ru: '',
    title_uz: '',
    subtitle_ru: '',
    subtitle_uz: '',
    hero_services_ru:[]
    })

    const getData = async () => {
        const result = await api.get(`layout/hero`)
        const projectsTitleResult = await api.get(`layout/main`);

        setMainData(projectsTitleResult?.data?.layout?.main);
        setHeroData(result?.data?.layout?.hero)
    }



    
    
    useEffect(() => {
        getData()
    }, [])


    return (
        <section className='hero '>
            <div   className="hero__top">
                    <div className='hero__top-left'>
                        <div className="hero__top-left-texts">
                            <h1>
                                {
                                    herData && language === 'uz' ? herData?.title_uz : herData?.title_ru || ' '
                                }
                              
                            </h1>
                            <p>
                                {
                                     herData && language === 'uz' ? herData?.subtitle_uz : herData?.subtitle_ru || ''
                                }
                                <img src={arrow} alt="" />
                            </p>
                        </div>
                        <button onClick={() => {
                            setOpen(true)
                            setChild('feedback')
                        }}>
                            {
                                language === 'uz'
                                    ? 'Ariza yuborish'
                                    : 'Получить консультацию'
                            }
                        </button>
                    </div>
                    <div className='hero__top-right' >
                        <img src={herData?.image || heroImg } alt="hero"/>
                    </div>
            </div>
            <div className="hero__bottom">
                <ul>
                    {
                    herData?.hero_services_ru && language === 'ru' &&  herData?.hero_services_ru.map((item, index) => (
                        <div key={index}>
                                <FaCheck size={25} fill={'#162850'}/>
                               <li>
                                <span style={{color: '#162850'}}>
                                {item.split(' ')[0]} {' '}
                                </span>
                                {item.split(' ').slice(1).map(text => (
                                    <span>{text} {'  '}</span>
                                ))}
                                </li>
                            </div>
                        ))
                    }

                    {
                    herData?.hero_services_uz && language === 'uz' && herData?.hero_services_uz.map((item, index) => (
                        <div key={index}>
                                <FaCheck size={25} fill={'#162850'}/>
                               <li>
                                <span style={{color: '#162850'}}>
                                {item.split(' ')[0]} {' '}
                                </span>
                                {item.split(' ').slice(1).map(text => (
                                    <span>{text} {'  '}</span>
                                ))}
                                </li>
                            </div>
                        ))
                    }
                </ul>
            </div>
            <div className='hero__bottom-contact contactAnim'>
            <a href={`tel:+${mainData?.primary_phone}`}>
                    <FaPhoneAlt size={25} fill={'#162850'}/>
                </a>
            </div>
        </section>
    )
}

export default Hero