import { useEffect, useState } from "react";
import contctImg from "../../assets/images/contact/contact-img.png";
import storage from "../../utils/helpers/storage";
import { api } from "../../services";
import { toast } from "react-toastify";
import Timer from "../ui/TImer";
// import {phoneEditor} from '../../hooks/usePhoneFormater'

interface IContactData {
  name: string;
  number: string;
}

const Contact = () => {
  const language = storage.getItem("language");
  const [contactForm, setContactForm] = useState<any>({
    title_ru: "",
    description_ru: "",
    title_uz: "",
    description_uz: "",
    image: "",
  });

  const [sendContactData, setSendContactData] = useState<IContactData>({
    name: "",
    number: "",
  });

  const getData = async () => {
    const result = await api.get(`layout/contact`);
    setContactForm(result?.data?.layout?.contact);
  };

  useEffect(() => {
    getData();
  }, []);

  const HandleChange = (e: any) => {
    setSendContactData((p) => ({ ...p, [e.target.name]: e.target.value }));
  };

  const sendContact = async (e: any) => {
    if (sendContactData.number.charAt(0) === "+") {
      sendContactData.number === sendContactData?.number?.slice(1);
    }

    e.preventDefault();

    const res = await api.post("contact", sendContactData, {
      headers: {
        token: storage.getItem("token"),
      },
    });

    if (res?.data?.status == 201) {
      toast.success(
        language === "uz"
          ? "sorovingiz uchun rahmat tez orada siz bilan mutahasis boglanadi"
          : "Спасибо за ваш запрос, специалист скоро свяжется с вами"
      );
    }
    setSendContactData({
      name: "",
      number: "",
    });
  };

  return (
    <section id="contact" className="contact">
      <div className="contact__container">
        <div className="contact__left">
          <div className="contact__left-top">
            {language === "uz" ? (
              <h2>
                {contactForm?.title_uz.split(" ")[0]}{" "}
                <span>
                  {contactForm?.title_uz.split(" ")[1]}{" "}
                  {contactForm?.title_uz.split(" ")[2]}
                </span>
              </h2>
            ) : (
              <h2>
                {contactForm?.title_ru.split(" ")[0]}{" "}
                <span>
                  {contactForm?.title_ru.split(" ")[1]}{" "}
                  {contactForm?.title_ru.split(" ")[2]}
                </span>
              </h2>
            )}

            <p>
              {language === "uz"
                ? contactForm?.description_uz
                : contactForm?.description_ru}
            </p>
            <br />
            <Timer />
          </div>
          <form className="contact__left-bottom" onSubmit={sendContact}>
            <div className="form__group field">
              <input
                type="text"
                value={sendContactData?.name}
                name="name"
                id="name"
                onChange={HandleChange}
                className="form__field"
                placeholder={language === "uz" ? "ismingiz" : "Ваше имя"}
                required
              />
              <label htmlFor="name" className="form__label">
                {language === "uz" ? "Ismingiz" : "Ваше имя"}
              </label>
            </div>
            <div className="form__group field">
              <input
                type="text"
                id="number"
                value={sendContactData?.number}
                name="number"
                onChange={HandleChange}
                className="form__field"
                placeholder={
                  language === "uz" ? "Telifon Raqamingiz" : "Номер телефона"
                }
                required
              />
              <label htmlFor="number" className="form__label">
                {language === "uz" ? "Telifon Raqamingiz" : "Номер телефона"}
              </label>
            </div>
            <button className="form__button">
              {language === "uz" ? "Ariza yuborish" : "Отправить заявку"}
            </button>
          </form>
        </div>
        <img
          className="contact__right"
          src={contactForm ? contactForm?.image : contctImg}
          alt="contact"
        />
      </div>
    </section>
  );
};

export default Contact;
