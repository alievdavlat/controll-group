// @ts-nocheck
import aboutImg from "../../assets/images/about/about.png";
import { useEffect, useState } from "react";
import { api } from "../../services";
import storage from "../../utils/helpers/storage";
import CountUp from 'react-countup';


interface IAboutData {
  image: string;
  title_ru: string;
  title_uz: string;
  subtitle_uz: string;
  subtitle_ru: string;
  counts: [
    {
        count:string,
        title_ru:string,
        title_uz:string
    }
  ];
}

const AboutUs = () => {
  const [countOn ,setCountOn] = useState(false)

  const language = storage.getItem("language");

  const [aboutData, setAboutData] = useState<IAboutData>({
    image: "",
    title_ru: "",
    title_uz: "",
    subtitle_uz: "",
    subtitle_ru: "",
    counts: [
        {
            count:'',
            title_ru:'',
            title_uz:''
        }
    ],
  });


  const getData = async () => {
    const result = await api.get(`layout/about`);
    setAboutData(result?.data?.layout?.about);
    
  };


  useEffect(() => {
    getData();
  }, []);

  return (
        <section className="about" id="about">
            
            <div  className="about__text">
              <div className="about__text-header">
                <h2>
                  {aboutData?.title_ru && aboutData?.title_uz && language == "uz"
                    ? aboutData?.title_uz || ""
                    : aboutData?.title_ru || ""}
                </h2>
                <p>
                  
                  {aboutData?.subtitle_ru &&
                  aboutData?.subtitle_uz &&
                  language == "uz"
                    ? aboutData?.subtitle_uz || ""
                    : aboutData?.subtitle_ru || ""}
                </p>
              </div>

              <ul className="about__text-footer">
                {
                
                  aboutData?.counts?.length && aboutData.counts.map((item, index) => (
                  <li key={index}>
                    <div className="about__text-footer--top">
                      <span>
                      <CountUp   
                      start={0}
                      end={item?.count} 
                      delay={0}
                      duration={2}
                      />
                        +
                      </span>

                      {/* {item?.count && item?.count} */}
                      <p>{item?.title_ru && item?.title_uz  && language === 'uz' ? item?.title_uz : item?.title_ru}</p>
                    </div>

                    <div
                      className={`about__text-footer--line ${
                        index === 3 && "remove"
                      } ${
                        window.innerWidth <= 470 && index === 1 && "remove"
                      } `}></div>
                  </li>
                ))}
              </ul>
            </div>

            <div   className="about__right">
              <img src={aboutData?.image || aboutImg} alt="about" />
            </div>
        </section>
  );
};

export default AboutUs;
