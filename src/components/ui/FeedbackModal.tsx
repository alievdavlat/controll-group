import React from 'react'
import {MdClose} from "react-icons/md";
import {SuccessLoader} from '..';
import storage from '../../utils/helpers/storage';
import { api } from '../../services';
import { toast } from 'react-toastify';

type Props = {
    setOpen: (open: boolean) => void;

}




const FeedbackModal: React.FC<Props> = ({setOpen}) => {
    const [showSuccess, setShowSuccess] = React.useState(false)
    const [number, setNumber] = React.useState('')
    const [name, setname] = React.useState('')

    const language = storage.getItem('language')

   




    const sendContact = async (e:any) => {
        e.preventDefault()
       
        setTimeout(() => {
            setOpen(false)
        }, 2500);

        if (number.charAt(0) === '+') {
            number === number?.slice(1)
        }
        
        const res = await api.post('contact', {name, number}, {
            headers:{
                token:storage.getItem('token')
            }
        })
        

        if (res?.data?.status == 201) {
            setShowSuccess(true)
            toast.success(language === 'uz' ? 'sorovingiz uchun rahmat tez orada siz bilan mutahasis boglanadi' :'Спасибо за ваш запрос, специалист скоро свяжется с вами')
        }
        
       
        setNumber('')
        setname('')
    }    
  

    return (
        <div className='feedback'>
            <div className='feedback_close' onClick={() => setOpen(false)}>
                <MdClose/>
            </div>
            <div className="feedback_content">
                <div className='feedback_header'>
                    <h1>
                       {
                        language === 'uz' ? "Ariza" :" Заявка"
                       }
                    </h1>

                    {
                        !showSuccess &&
                        <p>
                            {
                                language === 'uz' 
                                ? "Kontakt ma'lumotlaringizni qoldiring va menejerimiz siz bilan bog‘lanib, biz haqimizda batafsil ma'lumot beradi "
                                : "Оставьте свои контактные данные, и наш менеджер расскажет больше интересных деталей"
                            }
                        </p>
                    }
                </div>
                {
                    showSuccess
                        ? <SuccessLoader/>
                        : <form className='feedback_form' onSubmit={sendContact}>
                            <div>
                                <input type="text" name='name' value={name} onChange={(e) => setname(e.target.value)}
                                       placeholder = {`${language == 'uz' ? 'Ismingiz': 'Имя'}`} required/>
                            </div>
                            <div>
                                <input type="tel"  name='number' value={number} onChange={(e) => setNumber(e.target.value)}
                                       placeholder={`${language == 'uz' ? 'Telefon raqamingiz' : 'Номер телефона'}`} required/>
                            </div>

                            <button 
                                    disabled={name?.length !== 0 && number?.length !== 0 ? showSuccess : true}
                                >
                                {
                                    language === 'uz' ? 'Yuborish' :"Отправить"
                                }
                            </button>
                        </form>
                }

                <div className="feedback__footer">
                    {
                        showSuccess
                            ?
                            <p>
                               {
                                language === 'uz' 
                                ? 'Sizning so\'rovingiz qabul qilindi. Tez orada siz bilan bog\'lanamiz'
                                : ' Ваш запрос получен. мы свяжемся с вами в ближайшее время'
                                }
                            </p>
                            : <p>
                                {
                                    language === 'uz' 
                                    ? 'Men shaxsiy ma\'lumotlarimni qayta ishlashga roziman'
                                    :'Даю согласие на обработку моих персональных данных'
                                }
                            </p>

                    }
                </div>
            </div>
        </div>
    )
}

export default FeedbackModal