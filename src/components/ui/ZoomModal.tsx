// @ts-nocheck
import React, { useEffect, useRef } from "react";
import { MdClose } from "react-icons/md";
import { FaSearchPlus } from "react-icons/fa";
import { FaSearchMinus } from "react-icons/fa";


type Props = {
  open?: boolean;
  setOpen: (open: boolean) => void;
  zoomImages: any;
};

const ZoomModal: React.FC<Props> = ({ open, setOpen, zoomImages }) => {
  const [scale, setSecale] = React.useState(1);
  const [zoomImg, setZoomImg] = React.useState(zoomImages[0]?.original);
  const [position, setPosition] = React.useState({ x: 0, y: 0 });
  const [activeIndex, setActiveIndex] = React.useState(0);

  const imgRef = useRef(null)

  

  const handleClose = (e: any) => {
    if (e.target.id === "zoomModal-bg") {
      setOpen(false);
    }
  };


  const hanldeZoomIn = () => {
      setSecale((scale) => scale + 0.1)
  }

  const hanldeZoomOut = () => {
    if (scale > 0.30000000000000016) {
      setSecale((scale) => scale - 0.1)
    }
  }

  useEffect(() => {
    const image = imgRef?.current
    let isDragging = false
    let prevPosition = {x:0 , y:0}

    const handleMouseDown = (e:any) => {
      isDragging = true
      prevPosition = {x:e?.clientX, y:e?.clientY}
    }

    const mouseMove = (e:any) => {
      if (!isDragging) {
        return
      }
      const deltaX = e?.clientX - prevPosition.x
      const deltaY = e?.clientY - prevPosition.y
      prevPosition = {x:e?.clientX, y:e?.clientY}

      setPosition((position) => ({
        x:position.x + deltaX,
        y:position.y + deltaY
      }))
    }

    const mouseUp = (e:any) => {
      isDragging = false
    }

    image?.addEventListener('mousedown', handleMouseDown)
    image?.addEventListener('mousemove', mouseMove)
    image?.addEventListener('mouseup', mouseUp)

    return () => {
      image?.removeEventListener('mousedown', handleMouseDown)
      image?.removeEventListener('mousemove', mouseMove)
      image?.removeEventListener('mouseup', mouseUp)   
    }

  }, [imgRef, scale])

  return (
    <div className="zoomModal-bg" id="zoomModal-bg" onClick={handleClose}>
      <div className="zoomModal" id="zoomModal">
        <span className="zoomModal-close" onClick={() => setOpen(false)}>
          <MdClose size={24} />
        </span>

        <div className="zoomModal-image_wrapper">
          <div className="zoomModal-image_wrapper_img">
            <img
              ref={imgRef}
              style={{
                cursor:'move',
                transform:`scale(${scale}) translate(${position.x}px, ${position.y}px)`,

              }}
              src={
                zoomImg  || ''
              }
              alt="image"
            />
          </div>

          <div className="zoomModal-image_wrapper_thumbs">
            {zoomImages?.map((img: any, index: number) => (
              <img
              key={img?.original}
                className={`${index === activeIndex && "active_item"}`}
                src={
                  img?.original 
                }
                alt="images"
                onClick={() => {
                  setActiveIndex(index);
                  setZoomImg(img?.original);
                }}
              />
            ))}
          </div>
        </div>

        <div className="zoomModal_tools">
          <button onClick={hanldeZoomIn}>
            <FaSearchPlus />
          </button>

          <button onClick={hanldeZoomOut}>
          <FaSearchMinus />
          </button>
        </div>
      </div>
    </div>
  );
};

export default ZoomModal;
