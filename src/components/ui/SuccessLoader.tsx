const SuccessLoader = () => {
  return (
    <div className="loading">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="124"
        height="124"
        viewBox="0 0 124 124">
        <circle
          className="circle-loading"
          cx="62"
          cy="62"
          r="59"
          fill="none"
          stroke="#aaa"
          stroke-width="6px"></circle>
        <circle
          className="circle"
          cx="62"
          cy="62"
          r="59"
          fill="none"
          stroke="black"
          stroke-width="6px"
          stroke-linecap="round"></circle>
        <polyline
          className="check"
          points="73.56 48.63 57.88 72.69 49.38 62"
          fill="none"
          stroke="black"
          stroke-width="6px"
          stroke-linecap="round"></polyline>
      </svg>
    </div>
  );
};

export default SuccessLoader;
