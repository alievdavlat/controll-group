import { useDispatch } from "react-redux"
import { changeLang } from "../../redux/localeSlice"
import FlagRu from "../../assets/icons/FlagRu"
import FlagUz from "../../assets/icons/FlagUz"
import React from "react"

type Props =  {
  setOpen:(open:boolean) => void;
  
}
const LanguageModal:React.FC<Props> = ({setOpen}) => {

  const dispatch = useDispatch()

  

  return (
    <div className="languageModal">
      <span onClick={() =>  {
        setOpen(false)
        dispatch(changeLang('uz')
        )}}>
        <FlagUz/>
        UZB
        </span>
      <span onClick={() => {
        dispatch(changeLang('ru'))
        setOpen(false)
      }}>
        <FlagRu/>
        РУ
      </span>
    </div>
  )
}

export default LanguageModal