import React from "react";
import {IoMdClose} from "react-icons/io";


type Props = {
    children: any;
    child: string;
    setOpen: (open: boolean) => void;
}

const CustomModal: React.FC<Props> = ({children, setOpen, child}) => {

    const handleClose = (e: any) => {
        if (e.target.id === 'customModal') {
            setOpen(false)
        }

    }

    return (
        <div className='customModal' id="customModal" onClick={handleClose}>
            <div className="customModal_content">
                {
                    child === 'language' && <span className="close_icon" onClick={() => setOpen(false)}>
                      <IoMdClose/>
                    </span>
                }
                {children}
            </div>

        </div>
    )
}

export default CustomModal