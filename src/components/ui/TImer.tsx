import { useState, useEffect } from "react";

function Timer() {
  const [time, setTime] = useState({ hours: 16, minutes: 0, seconds: 0 });

  useEffect(() => {
    const interval = setInterval(() => {
      setTime((prevTime) => {
        let { hours, minutes, seconds } = prevTime;
        seconds--;
        if (seconds < 0) {
          seconds = 59;
          minutes--;
        }
        if (minutes < 0) {
          minutes = 59;
          hours--;
        }
        if (hours < 0) {
          hours = 16;
        }
        return { hours, minutes, seconds };
      });
    }, 1000);

    return () => clearInterval(interval);
  }, []);
  return (
    <>
      <p style={{ fontSize: "1.5rem" }}>
        {time.hours} : {time.minutes} : {time.seconds}
      </p>
    </>
  );
}

export default Timer;
