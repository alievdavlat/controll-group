export default {
  addItem: (key:string, data:any) => {
    window.localStorage.setItem(key, JSON.stringify(data))
  },

  getItem: (key:string) => {
    return JSON.parse((window.localStorage as any).getItem(key))
  },

  removeItem: (key:string) => {
    window.localStorage.removeItem(key)
  },

  clearStorage: () => {
    window.localStorage.clear()
  }


}