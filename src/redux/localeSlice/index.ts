import { createSlice } from "@reduxjs/toolkit";
import storage from "../../utils/helpers/storage";

const initialState = {
lang: storage.getItem('language') ||  'ru' 
}


const localeSlice = createSlice({
  name:'localeSlice',
  initialState,
  reducers:{
    changeLang(state, action) {
      state.lang = action.payload
      storage.addItem('language', action.payload)
    }
  }
})


export  const  { changeLang } = localeSlice.actions;

export default localeSlice.reducer;