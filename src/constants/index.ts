
import works1 from '../assets/images/works/Rectangle 3029.jpg'
import works2 from '../assets/images/works//Rectangle 3044.jpg'
import works3 from '../assets/images/works//Rectangle 3043.jpg'



const navbaritems = [

  {
    id:1,
    title_ru:'О Компании',
    title_uz:'Biz Xaqimizda',
    path:'about'
  },

  {
    id:2,
    title_ru:'Виды работ',
    title_uz:'Xizmatlar',
    path:'services'
  },
  {
    id:3,
    title_ru:'Проекты',
    title_uz:'Loyihalar',
    path:'projects'
  },
  {
    id:4,
    title_ru:'Отзывы',
    title_uz:'Sharhlar',
    path:'review'
  },
  {
    id:5,
    title_ru:'Контакты',
    title_uz:'Bog`lanish',
    path:'contact'
  }



]


const heroItems = [
  {
    id:1 ,
    title_ru:'Прозрачная смета известна еще до начала работ',
    title_uz:'Shaffof baho ish boshlanishidan oldin ma\'lum'
  },
  {
    id:2,
    title_ru:'Четкое соблюдение сроков',
    title_uz:'Belgilangan muddatlarga qat\'iy rioya qilish'
  },
  {
    id:3,
    title_ru:'Оплата работ по факту выполнения',
    title_uz:'Ish tugaganidan keyin ish uchun to\'lov'
  },
  
  {
    id:4,
    title_ru:'Специалисты узкого профиля',
    title_uz:'Quyi mutaxassislik bo\'yicha mutaxassislar'
  },
  
]


const aboutItems = [
  {
    id: 1 , 
    number:'15',
    text:'lorem , ipsum '
  },
  {
    id: 2 , 
    number:'90',
    text:'lorem , ipsum '
  },
  {
    id: 3 , 
    number:'4000',
    text:'lorem , ipsum '
  },
  {
    id: 4 , 
    number:'7000',
    text:'lorem , ipsum '
  }
]


const servicesItem = [
  {
    id:1,
    title:'Ремонт под ключ',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
    article:'Ремонт'
  },
  {
    id:2,
    title:'Строительство с нуля',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
    article:'Строительство'
  },
  {
    id:3,
    title:'Фасадные работы',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
    article:'Фасад'
  },
  {
    id:4,
    title:'Дизайн проект',
    description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
    article:'Дизайн'
  }
]

const advantagesItem = [
  {
    id:1, 
    num:'02',
    title:'Гарантия',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
  },
  {
    id:2, 
    num:'02',
    title:'Выгода',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
  },
  {
    id:3, 
    num:'03',
    title:'Скорость работы',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
  },
  {
    id:4, 
    num:'04',
    title:'Многолетний опыт',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
  }
]


const worksItem = [
  {
    id:1,
    images:[
      {
        img1: works1,
        img2: works2,
        img3: works3,
      }
    ],
    address:'Ремонт квартиры Ташкент, ул. А.Каххара',
    measurment:'87 ',
    Deadline:'3 месяца'
  },
  {
    id:2,
    images:[
      {
        img1: works1,
        img2: works2,
        img3: works3,

      }
    ],
    address:'Ремонт квартиры Ташкент, ул. А.Каххара',
    measurment:'87',
    Deadline:'3 месяца'
  },
  {
    id:3,
    images:[
      {
        img1: works1,
        img2: works2,
        img3: works3,

      }
    ],
    address:'Ремонт квартиры Ташкент, ул. А.Каххара',
    measurment:'87',
    Deadline:'3 месяца'
  },
  {
    id:4,
    images:[
      {
        img1: works1,
        img2: works2,
        img3: works3,

      }
    ],
    address:'Ремонт квартиры Ташкент, ул. А.Каххара',
    measurment:'87',
    Deadline:'3 месяца'
  },
  {
    id:5,
    images:[
      {
        img1: works1,
        img2: works2,
        img3: works3,

      }
    ],
    address:'Ремонт квартиры Ташкент, ул. А.Каххара',
    measurment:'87',
    Deadline:'3 месяца'
  },
  {
    id:6,
    images:[
      {
        img1: works1,
        img2: works2,
        img3: works3,

      }
    ],
    address:'Ремонт квартиры Ташкент, ул. А.Каххара',
    measurment:'87',
    Deadline:'3 месяца'
  }

]



const reviewItems = [
  {
    id:1,
    owner:'Олег Ларин',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s  Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
  },
  {
    id:2,
    owner:'Даврон Равшанов',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s  Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
  },
  {
    id:3,
    owner:'Сардор Муроджонов',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s  Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
  },
  {
    id:4,
    owner:'Олег Ларин',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s  Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
  },
  {
    id:5,
    owner:'Олег Ларин',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s  Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
  },
  {
    id:6,
    owner:'Даврон Равшанов',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s  Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
  },
  {
    id:7,
    owner:'Даврон Равшанов',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s  Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
  },
  {
    id:8,
    owner:'Сардор Муроджонов',
    descr:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s  Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
  }
]

export {
  navbaritems,
  heroItems,
  aboutItems,
  servicesItem,
  advantagesItem,
  worksItem,
  reviewItems
}