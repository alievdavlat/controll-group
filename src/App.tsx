import {
  AboutUs,
  Advantages,
  Contact,
  Footer,
  Header,
  Hero,
  Services,
  Works,
  CustomModal,
  LanguageModal,
  Sidebar,
  FeedbackModal,
  ZoomModal,
  VideoSection,
} from "./components";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import { useSelector } from "react-redux";
import { uz } from "./utils/locales/uz";
import { ru } from "./utils/locales/ru";
import React from "react";
import Reviews from "./components/sections/Reviews";
import Aksiyalar from "./components/sections/Aksiyalar";
import Faqs from "./components/sections/Faqs";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { api } from "./services";

const App = () => {
  const { lang } = useSelector((state: any) => state.locale);
  const [child, setChild] = React.useState("");
  const [open, setOpen] = React.useState(false);
  const [openSidebar, setOpenSidebar] = React.useState(false);
  const [zommModalOpen, setZoomModalOpen] = React.useState(false);
  const [zoomImages, setZoomImages] = React.useState([]);
  const [videoData, setVideoData] = React.useState("");

  const resources = {
    uz: {
      translation: uz,
    },
    ru: {
      translation: ru,
    },
  };

  i18n.use(initReactI18next).init({
    resources,
    lng: lang,
    fallbackLng: lang,
    interpolation: {
      escapeValue: false,
    },
  });

  const getData = async () => {
    try {
      const result = await api.get(`layout/video`);

      setVideoData(result?.data?.layout?.video);
    } catch {
      console.log("errror");
    }
  };

  React.useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <div className="home">
        {videoData && <VideoSection />}

        {zommModalOpen && (
          <ZoomModal
            open={zommModalOpen}
            setOpen={setZoomModalOpen}
            zoomImages={zoomImages}
          />
        )}
        {openSidebar && (
          <Sidebar
            setOpen={setOpen}
            setChild={setChild}
            child={child}
            openSidebar={openSidebar}
            setOpenSidebar={setOpenSidebar}
          />
        )}

        {open && (
          <CustomModal setOpen={setOpen} child={child}>
            {child === "language" && <LanguageModal setOpen={setOpen} />}

            {child === "feedback" ? <FeedbackModal setOpen={setOpen} /> : ""}
          </CustomModal>
        )}

        <header>
          <Header
            setOpen={setOpen}
            setChild={setChild}
            child={child}
            openSidebar={openSidebar}
            setOpenSidebar={setOpenSidebar}
          />
        </header>

        <main>
          <Hero setOpen={setOpen} setChild={setChild} />

          <AboutUs />

          <Services setOpen={setOpen} setChild={setChild} />

          <Contact />

          <Advantages />

          <Works
            setZoomImages={setZoomImages}
            zoomImages={zoomImages}
            setZoomModalOpen={setZoomModalOpen}
          />
          <Aksiyalar />
          <Faqs />

          <Reviews />

          <Contact />
        </main>

        <footer>
          <Footer setOpen={setOpen} setChild={setChild} />
        </footer>
        <div className="footer__bottom">
          <p>
            © CONTROL GROUP 2023.{" "}
            {lang == "uz"
              ? "Barcha huquqlar himoyalangan"
              : " Все права защищены"}
          </p>
          <p>
            created by{" "}
            <a href="https://www.novastudio.uz/" target="_blank">
              NOVAS
            </a>{" "}
            ft.{" "}
            <a href="https://meraki.uz/" target="_blank">
              Meraki
            </a>
          </p>
        </div>
      </div>

      <ToastContainer />
    </>
  );
};

export default App;
